FROM alpine as alpine

ENV DL_LINK="http://ftp.unicamp.br/pub/apache/flex/4.16.1/binaries/apache-flex-sdk-4.16.1-bin.tar.gz"
ENV DL_LINK="https://static.sistemafieg.org.br/getin/tools/apache-flex-sdk-4.16.1-bin.tar.gz"

RUN apk update \
  && apk add ca-certificates \
  && apk add curl

RUN  mkdir -p /flex_sdk \
  && curl -sSLk ${DL_LINK} -o /flex_sdk/apache-flex-sdk-4.16.1-bin.tar.gz

RUN tar zxf /flex_sdk/apache-flex-sdk-4.16.1-bin.tar.gz -C /flex_sdk \
  && rm -f /flex_sdk/apache-flex-sdk-4.16.1-bin.tar.gz

RUN mkdir /flex_sdk/playerglobal -p  \
  && curl -sSLk https://fpdownload.macromedia.com/get/flashplayer/updaters/32/playerglobal32_0.swc -o /flex_sdk/playerglobal/playerglobal32_0.swc 

RUN mkdir -p /flex_sdk/debugger  \
  && curl -sSLk https://fpdownload.macromedia.com/pub/flashplayer/updaters/32/flash_player_sa_linux_debug.x86_64.tar.gz -o /flex_sdk/debugger/flash_player_sa_linux_debug.x86_64.tar.gz \
  && tar zxf /flex_sdk/debugger/flash_player_sa_linux_debug.x86_64.tar.gz -C /flex_sdk/debugger/ \
  && rm -f /flex_sdk/debugger/flash_player_sa_linux_debug.x86_64.tar.gz


RUN curl -sSLk http://mirror.nbtelecom.com.br/apache//ant/binaries/apache-ant-1.9.15-bin.tar.gz -o /tmp/apache-ant-1.9.15-bin.tar.gz \
  && tar zxf /tmp/apache-ant-1.9.15-bin.tar.gz -C /usr/local/ \
  && rm -f /tmp/apache-ant-1.9.15-bin.tar.gz

FROM azul/zulu-openjdk-alpine:8

RUN apk update && apk add make curl

COPY --from=alpine /flex_sdk /flex_sdk
COPY --from=alpine /usr/local/apache-ant-1.9.15 /usr/local/ant

ENV FLEX_HOME=/flex_sdk/apache-flex-sdk-4.16.1-bin
ENV playerglobalHome=/flex_sdk/playerglobal
ENV PLAYERGLOBALHOME=/flex_sdk/playerglobal
ENV FLASH_PLAYER_EXE=/flex_sdk/debugger
ENV ANT_HOME=/usr/local/ant

ENV PATH="$PATH:$ANT_HOME/bin:/flex_sdk/apache-flex-sdk-4.16.1-bin/bin"

COPY flex-config.xml /flex_sdk/apache-flex-sdk-4.16.1-bin/frameworks/flex-config.xml

WORKDIR /app

#ENTRYPOINT ["/flex_sdk/apache-flex-sdk-4.16.1-bin/bin/mxmlc"]
